class ScoresController < ApplicationController
  def new; end

  def index
    @scores = Score.order(score_number: :desc)
  end

  def import
    @score_importer = Score.import(params[:file])
    redirect_to scores_path, notice: "Importado com sucesso."
  end
end
