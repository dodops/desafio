require "csv"

class Score < ApplicationRecord
  validates :user_name, :score_number, :date, :user_id, presence: true

  validates_numericality_of :score_number, less_than: 3126

  # Import a file and parses into the database
  # Should be a .csv file
  # See pontuacao1.csv for example
  # @return nil
  def self.import(file)
    CSV.foreach(file.path, col_sep: ";") do |row|
      Score.create date: row[0], user_name: row[1], user_id: row[2], score_number: row[3]
    end
  end
end
