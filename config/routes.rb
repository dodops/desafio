Rails.application.routes.draw do
  resources :scores, only: [:new, :index] do
    collection { post :import }
  end

  root 'scores#index'
end
