class CreateScores < ActiveRecord::Migration[5.2]
  def change
    create_table :scores do |t|
      t.date :date
      t.string :user_name
      t.integer :user_id
      t.integer :score_number

      t.timestamps
    end
  end
end
