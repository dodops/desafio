require 'rails_helper'

RSpec.describe NavbarHelper, type: :helper do
  describe "#navbar_link_to" do
    before do
      allow(controller).to receive(:params).and_return({controller: 'users'})
    end

    it 'adds active class according to :active_when' do
      tag = navbar_link_to("Test", "http://example.com", title: "foo", active_when: { controller: "users" })

      expect(tag).to eq('<li class="active">'\
                        '<a title="foo" href="http://example.com">Test</a>'\
                        "</li>")
    end

    it "honors regular expressions" do
      tag = navbar_link_to("Test", "http://example.com", active_when: { controller: /^user.*/ })

      expect(tag).to eq('<li class="active"><a href="http://example.com">Test</a></li>',)
    end

    it 'otherwise does not add active class' do
      tag = navbar_link_to("Home", "/", active_when: { controller: "home" })

      expect(tag).to eq('<li><a href="/">Home</a></li>')
    end
  end
end

