require 'rails_helper'

RSpec.describe ScoresController, type: :controller do

  before { @file = fixture_file_upload('pontuacao1.csv', 'text/csv') }

  describe "GET #index" do
    it "returns a success response" do
      get :index, params: {}
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}
      expect(response).to be_success
    end
  end


  describe "POST #import" do
    context "with valid params" do
      it "creates Scores" do
        expect {
          post :import, params: { file: @file }
        }.to change(Score, :count).by(6)
      end

      it "redirects to the scores listing page" do
        post :import, params: {file: @file}
        expect(response).to redirect_to(scores_url)
      end
    end
  end
end
