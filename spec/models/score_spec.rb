require 'rails_helper'

RSpec.describe Score, type: :model do
  context 'Validations' do
    it { is_expected.to validate_presence_of(:user_id) }
    it { is_expected.to validate_presence_of(:user_name) }
    it { is_expected.to validate_presence_of(:score_number) }
    it { is_expected.to validate_presence_of(:date) }
    it { is_expected.to validate_numericality_of(:score_number).is_less_than(3126) }
  end

  describe ".import" do
    it 'creates records based on file' do
      file = File.open('pontuacao1.csv', 'r')

      described_class.import(file)
      expect(described_class.find_by(user_name: 'Karla').user_id).to eq 123
    end
  end
end

