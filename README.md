# desafio

Aplicativo WEB Rails 5.

## Requisitos

O projeto depende das sequintes libs:

* Ruby 2.4.1, ou superior.
* [SQLite3][] deve ser instalado e rodando na porta default

## Getting started

### bin/setup

Execute o `bin/setup`. O script irá:

* Instalar as gems
* Criar copias do `.env` e `database.yml`
* Criar e migrar o banco de dados

### Execute!

1. Execute `bin/rake spec` para rodar os testes.
2. Execute `bin/rails s` para iniciar a aplicação.
3. O arquvio usado para teste se encontra na raiz do projeto `pontuacao1.csv` 

[rbenv]:https://github.com/sstephenson/rbenv
[redis]:http://redis.io
[Homebrew]:http://brew.sh
